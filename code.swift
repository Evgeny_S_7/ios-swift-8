import Foundation

// п.1-4
struct Car : Hashable {
    var brand: String 
    var year: Int 
    var trunkVolume: Double 
    var engineOn: Bool 
    var windowsOpen: Bool
    var filledTrunkVolume: Double
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(brand)
        hasher.combine(year)
        hasher.combine(trunkVolume)
        hasher.combine(engineOn)
        hasher.combine(windowsOpen)
        hasher.combine(filledTrunkVolume)
    }
    
    static func == (lhs: Car, rhs: Car) -> Bool {
        return lhs.brand == rhs.brand && lhs.year == rhs.year && lhs.trunkVolume == rhs.trunkVolume && lhs.engineOn == rhs.engineOn && lhs.windowsOpen == rhs.windowsOpen && lhs.filledTrunkVolume == rhs.filledTrunkVolume
    }
}

struct Truck : Hashable {
    var brand: String
    var year: Int
    var bodyVolume: Double
    var engineOn: Bool
    var windowsOpen: Bool 
    var filledBodyVolume: Double
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(brand)
        hasher.combine(year)
        hasher.combine(bodyVolume)
        hasher.combine(engineOn)
        hasher.combine(windowsOpen)
        hasher.combine(filledBodyVolume)
    }
    
    static func == (lhs: Truck, rhs: Truck) -> Bool {
        return lhs.brand == rhs.brand && lhs.year == rhs.year && lhs.bodyVolume == rhs.bodyVolume && lhs.engineOn == rhs.engineOn && lhs.windowsOpen == rhs.windowsOpen && lhs.filledBodyVolume == rhs.filledBodyVolume
    }
    
}

enum Action {
    case startEngine
    case stopEngine 
    case openWindows
    case closeWindows 
    case load(volume: Double)
    case unload(volume: Double)
}

extension Car {
    mutating func performAction(_ action: Action) {
        switch action {
        case .startEngine:
            engineOn = true
        case .stopEngine:
            engineOn = false 
        case .openWindows:
            windowsOpen = true
        case .closeWindows:
            windowsOpen = false
        case .load(let volume):
            guard filledTrunkVolume + volume <= trunkVolume else {
                print("Не хватает места")
                return
            }
            filledTrunkVolume += volume
        case .unload(let volume):
            guard filledTrunkVolume - volume >= 0 else {
                print("Слишком мало груза")
                return
            }
            filledTrunkVolume -= volume
        }
    }
}

extension Truck {
    mutating func performAction(_ action: Action) {
        switch action {
        case .startEngine:
            engineOn = true
        case .stopEngine:
            engineOn = false
        case .openWindows:
            windowsOpen = true
        case .closeWindows:
            windowsOpen = false 
        case .load(let volume):
            guard filledBodyVolume + volume <= bodyVolume else {
                print("Не хватает места")
                return
            }
            filledBodyVolume += volume
        case .unload(let volume):
            guard filledBodyVolume - volume >= 0 else {
                print("Слишком мало груза")
                return
            }
            filledBodyVolume -= volume
        }
    }
}

var lastochka = Car(brand: "Волга", year: 2010, trunkVolume: 600, engineOn: true, windowsOpen: true, filledTrunkVolume: 150)
var sinichka = Car(brand: "Ока", year: 2005, trunkVolume: 50, engineOn: false, windowsOpen: false, filledTrunkVolume: 0)
var ZIL600SIL = Truck(brand: "ЗИЛ", year: 2000, bodyVolume: 5000, engineOn: false, windowsOpen: true, filledBodyVolume: 5000)
var Buhanka = Truck(brand: "УАЗ", year: 2020, bodyVolume: 2000, engineOn: true, windowsOpen: false, filledBodyVolume: 500)

lastochka.performAction(.closeWindows) 
sinichka.performAction(.startEngine) 
ZIL600SIL.performAction(.load(volume: 2000)) 
Buhanka.performAction(.load(volume: 1000)) 

var dict = [lastochka: "lastochka", sinichka: "sinichka", ZIL600SIL: "ZIL600SIL", Buhanka: "Buhanka"] as [AnyHashable: String]

print(dict as AnyObject)


// п.5
class Programmer {
    func sleep() {
        print("Swift... Swift... Swift...")
    }
}

// здесь в функции создаётся константа, которая не уничтожается, если замыкание используется где-то ещё
func letSleep() -> () -> Void {
    let evgen = Programmer()

    let singing = {
        evgen.sleep()
        return
    }

    return singing
}

let sleepFunction = letSleep()
sleepFunction()

// здесь в функции используется слабый захват - то есть swift уже не обязательно хранить константу в памяти, 
// тк она может быть nil. Ничего не выведет благодаря опциональной цепочке ?.
/*func letSleep() -> () -> Void {
    let evgen = Programmer()

    let sleeping = { [weak evgen] in
        evgen?.sleep()
        return
    }

    return sleeping
}

let sleepFunction = letSleep()
sleepFunction()*/

// здесь слабый захват и обязательный возврат значения через !. - упадёт с ошибкой
/*func letSleep() -> () -> Void {
    let evgen = Programmer()

    let sleeping = { [weak evgen] in
        evgen!.sleep()
        return
    }

    return sleeping
}

let sleepFunction = letSleep()
sleepFunction()*/

// здесь используется неуправляемый захват - то есть мы указываем, 
// что элемент точно будет существовать и можем обойтись без опциональной цепочки.
// Это сокращает затраты на управление памятью, но нужно быть предельно осторожным
// В данном случае как раз имеем ситуацию, когда элемент в памяти храниться не будет и код упадёт в ошибку
/*func letSleep() -> () -> Void {
    let evgen = Programmer()

    let sleeping = { [unowned evgen] in
        evgen.sleep()
        return
    }

    return sleeping
}

let sleepFunction = letSleep()
sleepFunction()*/



// п.6 - ошибка была в отсутствии ключевого слова weak - слабый захват для применения опциональности
class Carrrrr {
    // Добавляем ключевое слово weak перед объявлением свойства driver
    weak var driver: Mannnn?
    
    deinit{ // Выведем в консоль сообщение о том, что объект удален
        print("машина удалена из памяти")
    }
}

class Mannnn {
    var myCar: Carrrrr?
    
    deinit{ // Выведем в консоль сообщение о том, что объект удален
        print("мужчина удален из памяти")
    }
}

// Объявим переменные как опциональные, чтобы иметь возможность им nil
var carrrr: Carrrrr? = Carrrrr()
var mannnn: Mannnn? = Mannnn()

// машина теперь имеет слабую ссылку на мужчину
carrrr?.driver = mannnn

// а мужчина на машину
mannnn?.myCar = carrrr

// присвоим nil переменным, удалим эти ссылки
carrrr = nil
mannnn = nil


// п.7 - здесь ошибка также в отсутствии опциональности - нужно было добавить слабый захват для паспорта
class Man {
    var pasport: (() -> Passport?)? // По заданию weak или unowned запрещено ставить
    
    deinit {
        // выведем в консоль сообщение о том, что объект удален
        print("мужчина удален из памяти")
    }
}

class Passport {
    let man: Man
    
    init(man: Man){
        self.man = man
    }
    
    deinit {
        // выведем в консоль сообщение о том, что объект удален
        print("паспорт удален из памяти")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.pasport = { [weak passport] in
    return passport 
}
passport = nil // объект еще не удален, его удерживает мужчина 
man = nil // Теперь удалены оба объекта*/